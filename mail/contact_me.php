<?php
// Check for empty fields
if(empty($_POST['name'])  		||
   empty($_POST['email']) 		||
   empty($_POST['phone']) 		||
   empty($_POST['message'])	||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
	echo "Não há argumentos apresentados!";
	return false;
   }
	
$name = $_POST['name'];
$email_address = $_POST['email'];
$phone = $_POST['phone'];
$message = $_POST['message'];
	
// Create the email and send the message
$to = 'henriquegilbertosouza@gmail.com'; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
$email_subject = "  $name Te Enviou uma mensagem pelo site";
$email_body = "Você recebeu uma nova mensagem através do Formulario do Site.\n\n"."Aqui estão os detalhes :\n\nNome: $name\n\nE-mail: $email_address\n\nTelefone : $phone\n\nMensagem:\n$message";
$headers = "From: Rustico-Auto@rusticomadeiras.hol.es\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
$headers .= "Reply-To: $email_address";	
mail($to,$email_subject,$email_body,$headers);
return true;			
?>